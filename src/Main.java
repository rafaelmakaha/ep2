import controller.*;
import view.*;

public class Main {
	// Inicialização da aplicação
	public static void main(String[] args) {
		MenuPrincipal menu = new MenuPrincipal();
		ControllerMenu ctrl = new ControllerMenu(menu);
		menu.setTitle("Aprenda QEE");
		menu.setVisible(true);
	}

}
