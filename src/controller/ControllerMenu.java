/**Controller para navegar entre os menus
 * da aplicação
 */

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.*;
import view.*;

public class ControllerMenu {

	private MenuPrincipal menu;
	
	public ControllerMenu(MenuPrincipal menu) {
		this.menu = menu;
		//Chamado dos action listeners dos botões do Menu
		this.menu.addBtnFluxPotFund(new Listener());
		this.menu.addBtnDistoroHarmnica(new Listener());
	}
	
	class Listener implements ActionListener{

		
		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			//Entra na opção 1 do menu
			if(comando.equals("Fluxo de Potência Fundamental")){
				ViewUC2 view = new ViewUC2();
				CalcUC2 uc2 = new CalcUC2();
				ControllerUC2 ctrl = new ControllerUC2(view, uc2);
				view.setTitle("Fluxo de Potência Fundamental");
				view.setVisible(true);
			}
			//Entra na opção 2 do menu
			if(comando.equals("Distorção Harmônica")) {
				ViewUC3 view = new ViewUC3();
				CalcUC3 uc3 = new CalcUC3();
				ControllerUC3 ctrl = new ControllerUC3(view, uc3);
				view.setTitle("Distorção Harmônica");
				view.setVisible(true);
			}
			
		}
		
	}
	
}
