package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.xml.bind.ValidationException;
import model.CalcUC2;
import view.ViewUC2;

public class ControllerUC2{

	private CalcUC2 uc2;
	private ViewUC2 view;
	
	public ControllerUC2(ViewUC2 view, CalcUC2 uc2) {
		this.view = view;
		this.uc2 = uc2;
		//Toda vez que o botão Calcular for clicado, irá executar o actionPerformed implementado abaixo
		this.view.addBtnCalcularListener(new CalcularListener());
		this.view.addBtnVoltarListener(new CalcularListener());
	}
	
	class CalcularListener implements ActionListener{
	
		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			if(comando.equals("Calcular")) {
				//Verificação dos dados inseridos pelo usuário
				try {
					//atualiza as variáveis na Model
					uc2.setAmplitudeTensao(view.getComboBoxAmplitudeTensao());
					uc2.setAmplitudeCorrente(view.getComboBoxAmplitudeCorrente());
					uc2.setAnguloTensao(view.getComboBoxAnguloTensao());
					uc2.setAnguloCorrente(view.getComboBoxAnguloCorrente());
					
					//atualiza os gráficos da view
					view.setGraficoCorrente(uc2.pontosOnda(uc2.getAmplitudeCorrente(), uc2.getAnguloCorrente()));
					view.setGraficoTensao(uc2.getPontosTensao());
					view.setGraficoPotenciaInstantanea(uc2.getPontosPotenciaInstantanea());
					view.setTrianguloPot(view.trianguloPot,(int)uc2.potenciaAtiva(),(int)uc2.potenciaReativa() );
					
					//atualiza as saídas da view
					view.setTextFatPot(String.valueOf(uc2.fatorPotencia()));
					view.setTextPotReativa(String.valueOf((int)uc2.potenciaReativa()));
					view.setTextPotAparente(String.valueOf((int)uc2.potenciaAparente()));
					view.setTextPotAtiva(String.valueOf((int)uc2.potenciaAtiva()));
					
					
				}catch(ValidationException e1){
					e1.printStackTrace();
					/**
					if(e1.getMessage().equals("Valor de ângulo de Fase de Tensão inválido")) {
						view.setLblErroTensao(e1.getMessage());
					}else if(e1.getMessage().equals("Valor de ângulo de Fase de Corrente inválido")) {
						view.setLblErroCorrente(e1.getMessage());
					}else if(e1.getMessage().equals("Valor de Amplitude de Tensão inválido")) {
						view.setLblErroTensao(e1.getMessage());
					}else if(e1.getMessage().equals("Valor de Amplitude de Corrente inválido")) {
						view.setLblErroCorrente(e1.getMessage());
					}
					*/
				}

			}
			if(comando.equals("Voltar")) {
				view.dispose();
			}
			
		}
	}
	
	
}
