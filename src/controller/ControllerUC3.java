package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.xml.bind.ValidationException;
import model.CalcUC3;
import view.ViewUC3;

public class ControllerUC3 {
	
	ViewUC3 view;
	CalcUC3 uc3;
	
	public ControllerUC3(ViewUC3 view, CalcUC3 uc3) {
		this.view = view;
		this.uc3 = uc3;
		
		//Action listeners dos botões da view
		this.view.addBtnCalcEntradaListener(new Listener());
		this.view.addBtnResultadoListener(new Listener());
		this.view.addBtnOkListener(new Listener());
		this.view.addBtnVoltarListener(new Listener());
	}
	
	class Listener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			if(comando.equals("Calcular")) {
				//Atualiza o gráfico
				view.setGraficoEntrada(uc3.pontosOnda(view.getComboBoxAmplitudeEntrada(), view.getComboBoxAnguloEntrada()));
				//Torna visível o segundo menu
				view.setVisibilidadeMenuHarmonico();				
			}
			if(comando.equals("Ok")) {
				try {
					//atualiza a tela com a quantidade de harmonicos escolhidos
					view.setComboBoxHarmonicos(view.getComboBoxParidadeHarm());
					view.atualizaVisibilidade(view.getComboBoxNumOrdemHarm());
					view.setVisibilidadeBtnResultado();
				}catch(ValidationException e1){
					e1.printStackTrace();
				}
				
			}
			if(comando.equals("Resultado")) {
				try {
					//gera os gráficos dos harmônicos e o gráfico resultante
					view.setGraficoH1(uc3.pontosOndaOrdemHarmonica(view.getComboBoxAmplitudeH1(), view.getComboBoxAnguloH1(), view.getComboBoxHarmH1()));
					view.setGraficoH2(uc3.pontosOndaOrdemHarmonica(view.getComboBoxAmplitudeH2(), view.getComboBoxAnguloH2(), view.getComboBoxHarmH2()));
					view.setGraficoH3(uc3.pontosOndaOrdemHarmonica(view.getComboBoxAmplitudeH3(), view.getComboBoxAnguloH3(), view.getComboBoxHarmH3()));
					view.setGraficoH4(uc3.pontosOndaOrdemHarmonica(view.getComboBoxAmplitudeH4(), view.getComboBoxAnguloH4(), view.getComboBoxHarmH4()));
					view.setGraficoH5(uc3.pontosOndaOrdemHarmonica(view.getComboBoxAmplitudeH5(), view.getComboBoxAnguloH5(), view.getComboBoxHarmH5()));
					view.setGraficoH6(uc3.pontosOndaOrdemHarmonica(view.getComboBoxAmplitudeH6(), view.getComboBoxAnguloH6(), view.getComboBoxHarmH6()));
					view.setGraficoSaida(uc3.pontosOndaResultante(view.getComboBoxAmplitudeEntrada(),
																	view.getComboBoxAmplitudeH1(), 
																	view.getComboBoxAmplitudeH2(), 
																	view.getComboBoxAmplitudeH3(), 
																	view.getComboBoxAmplitudeH4(), 
																	view.getComboBoxAmplitudeH5(),
																	view.getComboBoxAmplitudeH6(), 
																	view.getComboBoxAnguloEntrada(), 
																	view.getComboBoxAnguloH1(), 
																	view.getComboBoxAnguloH2(), 
																	view.getComboBoxAnguloH3(), 
																	view.getComboBoxAnguloH4(), 
																	view.getComboBoxAnguloH5(), 
																	view.getComboBoxAnguloH6(), 
																	view.getComboBoxHarmH1(), 
																	view.getComboBoxHarmH2(), 
																	view.getComboBoxHarmH3(), 
																	view.getComboBoxHarmH4(), 
																	view.getComboBoxHarmH5(), 
																	view.getComboBoxHarmH6()));
				}catch(ValidationException e1){
					e1.printStackTrace();
				}
			}
			//botão de retorno ao menu principal
			if(comando.equals("Voltar")) {
				view.dispose();
			}
			
		}
		
	}
}
