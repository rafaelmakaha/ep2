package model;

import java.util.ArrayList;

import javax.xml.bind.ValidationException;

public class CalcUC2 implements Onda {

	private double anguloTensao;
	private double anguloCorrente;
	private double amplitudeTensao;
	private double amplitudeCorrente;
	
	public CalcUC2() {
		this.anguloTensao = 0;
		this.amplitudeCorrente = 0;
		this.anguloCorrente = 0;
		this.amplitudeTensao = 0;
	}
	
	public double getAnguloTensao() {
		return anguloTensao;
	}
	public void setAnguloTensao(double anguloTensao) throws ValidationException {
		if(anguloTensao > 180 || anguloTensao < -180) {
			throw new ValidationException("Valor de ângulo de Fase de Tensão inválido");
		}else {
			this.anguloTensao = anguloTensao;
		}
	}
	public double getAnguloCorrente() {
		return anguloCorrente;
	}
	public void setAnguloCorrente(double anguloCorrente) throws ValidationException {
		if(anguloCorrente > 180 || anguloCorrente < -180) {
			throw new ValidationException("Valor de ângulo de Fase de Corrente inválido");
		}else {
			this.anguloCorrente = anguloCorrente;
		}
	}
	public double getAmplitudeTensao() {
		return amplitudeTensao;
	}
	public void setAmplitudeTensao(double amplitudeTensao) throws ValidationException {
		if(amplitudeTensao > 220 || amplitudeTensao < 0) {
			throw new ValidationException("Valor de Amplitude de Tensão inválido");
		}else {
			this.amplitudeTensao = amplitudeTensao;
		}
	}
	public double getAmplitudeCorrente() {
		return amplitudeCorrente;
	}
	public void setAmplitudeCorrente(double amplitudeCorrente) throws ValidationException {
		if(amplitudeCorrente > 100 || amplitudeCorrente < 0) {
			throw new ValidationException("Valor de Amplitude de Corrente inválido");
		}else {
			this.amplitudeCorrente = amplitudeCorrente;
		}
	}
	
	public double potenciaAtiva() {
		return getAmplitudeTensao() * getAmplitudeCorrente() * Math.cos(Math.toRadians(getAnguloTensao() - getAnguloCorrente()));
	}
	
	public double potenciaReativa() {
		return getAmplitudeTensao() * getAmplitudeCorrente() * Math.sin(Math.toRadians(getAnguloTensao() - getAnguloCorrente()));
	}
	
	public double potenciaAparente() {
		return (getAmplitudeTensao()) * (getAmplitudeCorrente());
	}
	
	public double fatorPotencia() {
		return Math.cos(Math.toRadians(getAnguloTensao() - getAnguloCorrente()));
	}
	public double freqAngular() {
		return 2 * 3.14 * 60;
	}
	public ArrayList<Double> getPontosTensao(){// Retorna pontos da Onda
		
		ArrayList<Double> valorTensao = new ArrayList<Double>();
		
		for( double t = 0; t < 101; t += 0.1) {
			valorTensao.add(getAmplitudeTensao() * Math.cos(freqAngular() * t + getAnguloTensao()));
		}
		return valorTensao;
	}

	public ArrayList<Double> pontosOnda(double tensao, double angulo){// Retorna pontos da Onda
		
		ArrayList<Double> valorCorrente = new ArrayList<Double>();
		
		for( double t = 0; t < 101; t += 0.1) {
			valorCorrente.add(getAmplitudeCorrente() * Math.cos(freqAngular() * t + getAnguloCorrente()));
		}
		return valorCorrente;
	}

	public ArrayList<Double> getPontosPotenciaInstantanea(){ // Retorna pontos da Onda
		
		ArrayList<Double> valorPotenciaInstantanea = new ArrayList<Double>();
		
		for( double t = 0; t < 101; t += 0.1) {
			valorPotenciaInstantanea.add(getAmplitudeTensao() * Math.cos(freqAngular() * t + getAnguloTensao())* (getAmplitudeCorrente() * Math.cos(freqAngular() * t + getAnguloCorrente())));
		}
		return valorPotenciaInstantanea;

	}
	
}
