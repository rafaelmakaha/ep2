package model;

import java.util.ArrayList;

import javax.xml.bind.ValidationException;

public class CalcUC3 implements Onda{
	private double freqAng = 2 * 3.14 * 60;
	
	
	public ArrayList<Double> pontosOnda(double tensao, double angulo){
				
		ArrayList<Double> pontosOnda = new ArrayList<Double>();
		
		for(double i = 0; i < 101; i += 0.1) {
			pontosOnda.add( tensao * Math.cos(freqAng * i  + angulo));
		}
		
		return pontosOnda;
	}
	
	//Retorna um Array com os pontos para UMA Onda Harmônica
	public ArrayList<Double> pontosOndaOrdemHarmonica(double tensao, double angulo, double harmonico) throws ValidationException{
		if(tensao < 0 || tensao > 220 || angulo < -180 || angulo > 180) {
			throw new ValidationException("Valor de ângulo de Fase de Tensão inválido");
		}
		
		ArrayList<Double> pontosOnda = new ArrayList<>();
		
		for(double i = 0; i < 101; i += 0.1) {
			pontosOnda.add( tensao * Math.cos(harmonico * freqAng * i  + angulo));
		}
		return pontosOnda;
	}
	
	//Retorna um array com os pontos da Onda resultante
	public ArrayList<Double> pontosOndaResultante(double tensaoEntrada,
													double tensaoh1,
													double tensaoh2,
													double tensaoh3,
													double tensaoh4,
													double tensaoh5,
													double tensaoh6,
													double anguloEntrada,
													double anguloh1,
													double anguloh2,
													double anguloh3,
													double anguloh4,
													double anguloh5,
													double anguloh6,
													double harmonico1,
													double harmonico2,
													double harmonico3,
													double harmonico4,
													double harmonico5,
													double harmonico6){
		
		ArrayList<Double> pontosOnda = new ArrayList<>();
		
		for(double i = 0; i < 101; i += 0.1) {
			pontosOnda.add( tensaoEntrada * Math.cos(freqAng * i  + anguloEntrada) +
							tensaoh1 * Math.cos(harmonico1 * freqAng * i + anguloh1) +
							tensaoh2 * Math.cos(harmonico2 * freqAng * i + anguloh2) +
							tensaoh3 * Math.cos(harmonico3 * freqAng * i + anguloh3) +
							tensaoh4 * Math.cos(harmonico4 * freqAng * i + anguloh4) +
							tensaoh5 * Math.cos(harmonico5 * freqAng * i + anguloh5) +
							tensaoh6 * Math.cos(harmonico6 * freqAng * i + anguloh6));
		}
		return pontosOnda;
		
	}
}
