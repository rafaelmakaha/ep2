package model;

import java.util.ArrayList;

public interface Onda {

	//Método comum as classes da Model
	public ArrayList<Double> pontosOnda(double tensao, double angulo);
}
