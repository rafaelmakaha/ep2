package teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.CalcUC2;

class CalcUC2Teste {

	CalcUC2 uc2;
	
	@BeforeEach
	void setUp() throws Exception {
		uc2 = new CalcUC2();
		uc2.setAmplitudeCorrente(39);
		uc2.setAmplitudeTensao(220);
		uc2.setAnguloCorrente(35);
		uc2.setAnguloTensao(0);
	}

	@Test
	void testPotenciaAtiva() {
		assertEquals(7028, (int)uc2.potenciaAtiva());
	}

	@Test
	void testPotenciaReativa() {
		assertEquals(-4921, (int)uc2.potenciaReativa());
	}

	@Test
	void testPotenciaAparente() {
		assertEquals(8580,uc2.potenciaAparente());
	}


}
