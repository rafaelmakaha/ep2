package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

public class MenuPrincipal extends JFrame {

	private JPanel contentPane;
	private JButton btnFluxPotFund;
	private JButton btnDistoroHarmnica;

	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 450, 300);
		contentPane.add(panel);
		
		JLabel lblAprendaQee = new JLabel("Aprenda QEE");
		lblAprendaQee.setBounds(171, 33, 116, 15);
		panel.add(lblAprendaQee);
		
		btnFluxPotFund = new JButton("Fluxo de Potência Fundamental");
		btnFluxPotFund.setBounds(40, 105, 377, 44);
		panel.add(btnFluxPotFund);
		
		btnDistoroHarmnica = new JButton("Distorção Harmônica");
		btnDistoroHarmnica.setBounds(40, 180, 377, 44);
		panel.add(btnDistoroHarmnica);
	}
	
	public void addBtnFluxPotFund(ActionListener Listener) {
		btnFluxPotFund.addActionListener(Listener);
	}
	public void addBtnDistoroHarmnica(ActionListener Listener) {
		btnDistoroHarmnica.addActionListener(Listener);
	}
}
