package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TrianguloPot extends JPanel{

	    private Color lineColor = new Color(44, 102, 230, 180);
	    private int potAtiva;
	    private int potReativa;
	    
	    public TrianguloPot(int valor_potenciaativa, int valor_potenciareativa) {
	        this.potAtiva = valor_potenciaativa/100;
	        this.potReativa = valor_potenciareativa/100;
	    }
	    
	    protected void paintComponent(Graphics g) {
	        Graphics2D g2 = (Graphics2D) g;
	        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	        g2.setColor(Color.WHITE);
	        g2.fillRect(50,0,360,300);
	        g2.setColor(Color.BLACK);

	        g2.drawLine(50, 150,410, 150);
	        g2.drawLine(230, 0, 230 , 300 );
	        g2.setColor(lineColor.PINK); 																
	        g2.drawLine(230,150,230 + potAtiva,150 );
		    g2.setColor(lineColor.GREEN);
		    g2.drawLine(230 + potAtiva,150,230 + potAtiva,150 - potReativa);
		    g2.setColor(lineColor.CYAN);
		    g2.drawLine(230 ,150,230 + potAtiva,150 - potReativa );
	       
	}

}
