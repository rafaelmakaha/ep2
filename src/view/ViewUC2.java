package view;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.CalcUC2;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ViewUC2 extends JFrame {

	private JPanel contentPane;
	private JTextField textPotAtiva;
	private JTextField textPotReativa;
	private JTextField textPotAparente;
	private JTextField textFatPot;
	private JPanel panel_3;
	private JPanel panel_4;
	private JPanel panel_6;
	private JPanel panel_7;
	private JComboBox<Double> comboBoxAmplitudeTensao;
	private JComboBox<Double> comboBoxAmplitudeCorrente;
	private JComboBox<Double> comboBoxAnguloTensao;
	private JComboBox<Double> comboBoxAnguloCorrente;
	private JButton btnCalcular;
	private CalcUC2 uc2;
	private GraphPanel graficoTensao;
	private GraphPanel graficoCorrente;
	private GraphPanel graficoPotenciaInstantanea;
	public TrianguloPot trianguloPot;
	private JButton btnVoltar;

	public ViewUC2() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // Dispose no lugar de close
		setBounds(0, 0, 1024, 720);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		uc2 = new CalcUC2();
		ArrayList<Double> zeros = new ArrayList<>();
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(12, 12, 1000, 346);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(1, 3, 0, 0));
		
		JPanel panel = new JPanel();
		panel_1.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblAmplitudeTensao = new JLabel("Amplitude de Tensão");
		GridBagConstraints gbc_lblAmplitudeTensao = new GridBagConstraints();
		gbc_lblAmplitudeTensao.insets = new Insets(0, 0, 5, 5);
		gbc_lblAmplitudeTensao.gridx = 0;
		gbc_lblAmplitudeTensao.gridy = 1;
		panel.add(lblAmplitudeTensao, gbc_lblAmplitudeTensao);
		
		comboBoxAmplitudeTensao = new JComboBox<Double>();
		for (double i = 0; i < 221; i++){
            comboBoxAmplitudeTensao.addItem(i);
        }
		GridBagConstraints gbc_comboBoxAmplitudeTensao = new GridBagConstraints();
		gbc_comboBoxAmplitudeTensao.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxAmplitudeTensao.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxAmplitudeTensao.gridx = 3;
		gbc_comboBoxAmplitudeTensao.gridy = 1;
		panel.add(comboBoxAmplitudeTensao, gbc_comboBoxAmplitudeTensao);
		
		JLabel lblAnguloTensao = new JLabel("Angulo de Fase");
		GridBagConstraints gbc_lblAnguloTensao = new GridBagConstraints();
		gbc_lblAnguloTensao.insets = new Insets(0, 0, 5, 5);
		gbc_lblAnguloTensao.gridx = 0;
		gbc_lblAnguloTensao.gridy = 3;
		panel.add(lblAnguloTensao, gbc_lblAnguloTensao);
		
		comboBoxAnguloTensao = new JComboBox<Double>();
		for (double i = -180; i < 181; i++){
            comboBoxAnguloTensao.addItem(i);
        }
		GridBagConstraints gbc_comboBoxAnguloTensao = new GridBagConstraints();
		gbc_comboBoxAnguloTensao.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxAnguloTensao.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxAnguloTensao.gridx = 3;
		gbc_comboBoxAnguloTensao.gridy = 3;
		panel.add(comboBoxAnguloTensao, gbc_comboBoxAnguloTensao);
		
		JLabel lblAmplitudeCorrente = new JLabel("Amplitude de Corrente");
		GridBagConstraints gbc_lblAmplitudeCorrente = new GridBagConstraints();
		gbc_lblAmplitudeCorrente.insets = new Insets(0, 0, 5, 5);
		gbc_lblAmplitudeCorrente.gridx = 0;
		gbc_lblAmplitudeCorrente.gridy = 5;
		panel.add(lblAmplitudeCorrente, gbc_lblAmplitudeCorrente);
		
		comboBoxAmplitudeCorrente = new JComboBox<Double>();
		for (double i = 0; i < 101; i++){
            comboBoxAmplitudeCorrente.addItem(i);
        }
		GridBagConstraints gbc_comboBoxAmplitudeCorrente = new GridBagConstraints();
		gbc_comboBoxAmplitudeCorrente.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxAmplitudeCorrente.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxAmplitudeCorrente.gridx = 3;
		gbc_comboBoxAmplitudeCorrente.gridy = 5;
		panel.add(comboBoxAmplitudeCorrente, gbc_comboBoxAmplitudeCorrente);
		
		JLabel lblAnguloCorrente = new JLabel("Angulo de Fase");
		GridBagConstraints gbc_lblAnguloCorrente = new GridBagConstraints();
		gbc_lblAnguloCorrente.insets = new Insets(0, 0, 5, 5);
		gbc_lblAnguloCorrente.gridx = 0;
		gbc_lblAnguloCorrente.gridy = 7;
		panel.add(lblAnguloCorrente, gbc_lblAnguloCorrente);
		
		comboBoxAnguloCorrente = new JComboBox<Double>();
		for (double i = -180; i < 181; i++){
            comboBoxAnguloCorrente.addItem(i);
        }
		GridBagConstraints gbc_comboBoxAnguloCorrente = new GridBagConstraints();
		gbc_comboBoxAnguloCorrente.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxAnguloCorrente.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxAnguloCorrente.gridx = 3;
		gbc_comboBoxAnguloCorrente.gridy = 7;
		panel.add(comboBoxAnguloCorrente, gbc_comboBoxAnguloCorrente);
		
		btnCalcular = new JButton("Calcular");
		GridBagConstraints gbc_btnCalcular = new GridBagConstraints();
		gbc_btnCalcular.insets = new Insets(0, 0, 5, 0);
		gbc_btnCalcular.gridx = 3;
		gbc_btnCalcular.gridy = 9;
		panel.add(btnCalcular, gbc_btnCalcular);
		
		panel_3 = new JPanel();
		panel_3.setLayout(new GridLayout(1, 1));
		graficoTensao = new GraphPanel(zeros);
		panel_3.add(graficoTensao);
		panel_1.add(panel_3);
		
		panel_4 = new JPanel();
		panel_4.setLayout(new GridLayout(1, 1));
		graficoCorrente = new GraphPanel(zeros);
		panel_4.add(graficoCorrente);
		panel_1.add(panel_4);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(12, 357, 1000, 346);
		contentPane.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_2.add(panel_5);
		GridBagLayout gbl_panel_5 = new GridBagLayout();
		gbl_panel_5.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_5.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_5.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_5.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_5.setLayout(gbl_panel_5);
		
		JLabel lblSaidas = new JLabel("SAIDAS");
		GridBagConstraints gbc_lblSaidas = new GridBagConstraints();
		gbc_lblSaidas.insets = new Insets(0, 0, 5, 5);
		gbc_lblSaidas.gridx = 1;
		gbc_lblSaidas.gridy = 0;
		panel_5.add(lblSaidas, gbc_lblSaidas);
		
		JLabel lblPotenciaAtiva = new JLabel("Potencia Ativa");
		GridBagConstraints gbc_lblPotenciaAtiva = new GridBagConstraints();
		gbc_lblPotenciaAtiva.insets = new Insets(0, 0, 5, 5);
		gbc_lblPotenciaAtiva.gridx = 0;
		gbc_lblPotenciaAtiva.gridy = 1;
		panel_5.add(lblPotenciaAtiva, gbc_lblPotenciaAtiva);
		
		textPotAtiva = new JTextField();
		textPotAtiva.setEditable(false);
		GridBagConstraints gbc_textPotAtiva = new GridBagConstraints();
		gbc_textPotAtiva.insets = new Insets(0, 0, 5, 0);
		gbc_textPotAtiva.fill = GridBagConstraints.HORIZONTAL;
		gbc_textPotAtiva.gridx = 2;
		gbc_textPotAtiva.gridy = 1;
		panel_5.add(textPotAtiva, gbc_textPotAtiva);
		textPotAtiva.setColumns(10);
		
		JLabel lblPotenciaReativa = new JLabel("Potencia Reativa");
		GridBagConstraints gbc_lblPotenciaReativa = new GridBagConstraints();
		gbc_lblPotenciaReativa.insets = new Insets(0, 0, 5, 5);
		gbc_lblPotenciaReativa.gridx = 0;
		gbc_lblPotenciaReativa.gridy = 3;
		panel_5.add(lblPotenciaReativa, gbc_lblPotenciaReativa);
		
		textPotReativa = new JTextField();
		textPotReativa.setEnabled(true);
		textPotReativa.setEditable(false);
		textPotReativa.setText("");
		GridBagConstraints gbc_textPotReativa = new GridBagConstraints();
		gbc_textPotReativa.insets = new Insets(0, 0, 5, 0);
		gbc_textPotReativa.fill = GridBagConstraints.HORIZONTAL;
		gbc_textPotReativa.gridx = 2;
		gbc_textPotReativa.gridy = 3;
		panel_5.add(textPotReativa, gbc_textPotReativa);
		textPotReativa.setColumns(10);
		
		JLabel lblPotenciaAparente = new JLabel("Potencia Aparente");
		GridBagConstraints gbc_lblPotenciaAparente = new GridBagConstraints();
		gbc_lblPotenciaAparente.insets = new Insets(0, 0, 5, 5);
		gbc_lblPotenciaAparente.gridx = 0;
		gbc_lblPotenciaAparente.gridy = 5;
		panel_5.add(lblPotenciaAparente, gbc_lblPotenciaAparente);
		
		textPotAparente = new JTextField();
		textPotAparente.setEditable(false);
		GridBagConstraints gbc_textPotAparente = new GridBagConstraints();
		gbc_textPotAparente.insets = new Insets(0, 0, 5, 0);
		gbc_textPotAparente.fill = GridBagConstraints.HORIZONTAL;
		gbc_textPotAparente.gridx = 2;
		gbc_textPotAparente.gridy = 5;
		panel_5.add(textPotAparente, gbc_textPotAparente);
		textPotAparente.setColumns(10);
		
		JLabel lblFatorPotencia = new JLabel("Fator Potencia");
		GridBagConstraints gbc_lblFatorPotencia = new GridBagConstraints();
		gbc_lblFatorPotencia.insets = new Insets(0, 0, 5, 5);
		gbc_lblFatorPotencia.gridx = 0;
		gbc_lblFatorPotencia.gridy = 7;
		panel_5.add(lblFatorPotencia, gbc_lblFatorPotencia);
		
		textFatPot = new JTextField();
		textFatPot.setEditable(false);
		GridBagConstraints gbc_textFatPot = new GridBagConstraints();
		gbc_textFatPot.insets = new Insets(0, 0, 5, 0);
		gbc_textFatPot.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFatPot.gridx = 2;
		gbc_textFatPot.gridy = 7;
		panel_5.add(textFatPot, gbc_textFatPot);
		textFatPot.setColumns(10);
		
		btnVoltar = new JButton("Voltar");
		GridBagConstraints gbc_btnVoltar = new GridBagConstraints();
		gbc_btnVoltar.insets = new Insets(0, 0, 0, 5);
		gbc_btnVoltar.gridx = 0;
		gbc_btnVoltar.gridy = 11;
		panel_5.add(btnVoltar, gbc_btnVoltar);
		
		panel_6 = new JPanel();
		panel_6.setLayout(new GridLayout(1, 1));
		//graficoPotenciaInstantanea = null;
		graficoPotenciaInstantanea = new GraphPanel(zeros);
		panel_6.add(graficoPotenciaInstantanea);
		panel_2.add(panel_6);
		
		panel_7 = new JPanel();
		panel_7.setLayout(null);
		trianguloPot = new TrianguloPot(0,0);
		trianguloPot.setBounds(-69, 12, 390, 322);
		panel_2.add(panel_7);
		panel_7.add(trianguloPot);
		
		JLabel lblEntradas = new JLabel("ENTRADAS");
		lblEntradas.setBounds(110, 0, 94, 15);
		contentPane.add(lblEntradas);
	}
	//Métodos acessores para manipulação das informações
	public void setTrianguloPot(TrianguloPot trianguloPot, int potAtiva, int potReativa) {
		panel_7.removeAll();
		trianguloPot = new TrianguloPot(potAtiva, potReativa);
		trianguloPot.setBounds(-69, 12, 390, 322);
		panel_7.add(trianguloPot);
		trianguloPot.revalidate();
		trianguloPot.repaint();
	}
	public void setTextPotAtiva(String textPotAtiva) {
		this.textPotAtiva.setText(textPotAtiva + " Watt");
	}
	public void setTextPotReativa(String textPotReativa) {
		this.textPotReativa.setText(textPotReativa + " VAR");
	}
	public void setTextPotAparente(String textPotAparente) {
		this.textPotAparente.setText(textPotAparente + " VA");
	}
	public void setTextFatPot(String textFatPot) {
		this.textFatPot.setText(textFatPot);
	}
	public void setGraficoTensao(ArrayList<Double> pontosTensao) {
		panel_3.removeAll();
		graficoTensao = new GraphPanel(pontosTensao);
		panel_3.add(graficoTensao);
		graficoCorrente.revalidate();
		graficoCorrente.repaint();
	}
	public void setGraficoCorrente(ArrayList<Double> pontosCorrente) {
		panel_4.removeAll();
		graficoCorrente = new GraphPanel(pontosCorrente);
		panel_4.add(graficoCorrente);
		graficoCorrente.revalidate();
		graficoCorrente.repaint();
	}
	public void setGraficoPotenciaInstantanea(ArrayList<Double> pontosPotenciaInstantanea) {
		panel_6.removeAll();
		graficoPotenciaInstantanea = new GraphPanel(pontosPotenciaInstantanea);
		panel_6.add(graficoPotenciaInstantanea);
		graficoPotenciaInstantanea.revalidate();
		graficoPotenciaInstantanea.repaint();
	}
	public double getComboBoxAmplitudeTensao() {
		return (double) comboBoxAmplitudeTensao.getSelectedItem();
	}

	public double getComboBoxAnguloTensao() {
		return (double) comboBoxAnguloTensao.getSelectedItem();
	}

	public double getComboBoxAnguloCorrente() {
		return (double) comboBoxAnguloCorrente.getSelectedItem();
	}

	public double getComboBoxAmplitudeCorrente(){
		return (double) comboBoxAmplitudeCorrente.getSelectedItem();
	}
	
	//Chamados dos Listeners dos Botões
	public void addBtnCalcularListener(ActionListener calcularListener) {
		btnCalcular.addActionListener(calcularListener);
	}
	public void addBtnVoltarListener(ActionListener calcularListener) {
		btnVoltar.addActionListener(calcularListener);
	}
}
