package view;

import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.bind.ValidationException;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class ViewUC3 extends JFrame {

	private JPanel contentPane;
	private JPanel panel_3;
	private JPanel panel_4;
	private JPanel panel_5;
	private JPanel panel_6;
	private JPanel panel_7;
	private JPanel panel_8;
	private JPanel panel_9;
	private JPanel panel_10;
	private JPanel panel_11;
	private JPanel panel_12;
	private JComboBox<Double> comboBoxAmplitudeEntrada;
	private JComboBox<Double> comboBoxAnguloEntrada;
	private JComboBox<Integer> comboBoxNumOrdemHarm;
	private JComboBox<String> comboBoxParidadeHarm;
	private JComboBox<Double> comboBoxAmplitudeH1;
	private JComboBox<Double> comboBoxAnguloH1;
	private JComboBox<Double> comboBoxHarmH1;
	private JComboBox<Double> comboBoxAmplitudeH2;
	private JComboBox<Double> comboBoxAnguloH2;
	private JComboBox<Double> comboBoxHarmH2;
	private JComboBox<Double> comboBoxAmplitudeH3;
	private JComboBox<Double> comboBoxAnguloH3;
	private JComboBox<Double> comboBoxHarmH3;
	private JComboBox<Double> comboBoxAmplitudeH4;
	private JComboBox<Double> comboBoxAnguloH4;
	private JComboBox<Double> comboBoxHarmH4;
	private JComboBox<Double> comboBoxAmplitudeH5;
	private JComboBox<Double> comboBoxAnguloH5;
	private JComboBox<Double> comboBoxHarmH5;
	private JComboBox<Double> comboBoxAmplitudeH6;
	private JComboBox<Double> comboBoxAnguloH6;
	private JComboBox<Double> comboBoxHarmH6;
	private GraphPanel graficoEntrada;
	private GraphPanel graficoSaida;
	private GraphPanel graficoH1;
	private GraphPanel graficoH2;
	private GraphPanel graficoH3;
	private GraphPanel graficoH4;
	private GraphPanel graficoH5;
	private GraphPanel graficoH6;
	private JButton btnResultado;
	private JButton btnCalcEntrada;
	private JButton btnOk;
	private JButton btnVoltar;
	private JLabel lblNewLabel_1;
	

	public ViewUC3() {
		ArrayList<Double> zeros = new ArrayList<>(); // Array zerado para inicializar os gráficos
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // Dispose ao invés de fechar
		setBounds(0, 0, 1024, 960);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 0, 1000, 240);
		panel.setLayout(new GridLayout(1, 3));
		contentPane.add(panel);
		
		panel_4 = new JPanel();
		panel_4.setLayout(null);
		graficoEntrada = new GraphPanel(zeros);
		graficoEntrada.setBounds(12, 27, 297, 161);
		panel_4.add(graficoEntrada);
		panel.add(panel_4);
		
		panel_5 = new JPanel();
		panel.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel label = new JLabel("Amplitude");
		label.setBounds(12, 98, 71, 15);
		panel_5.add(label);
		
		comboBoxAmplitudeEntrada = new JComboBox<Double>();
		comboBoxAmplitudeEntrada.setBounds(88, 94, 71, 24);
		panel_5.add(comboBoxAmplitudeEntrada);
		
		JLabel label_1 = new JLabel("Angulo");
		label_1.setBounds(34, 127, 49, 15);
		panel_5.add(label_1);
		
		comboBoxAnguloEntrada = new JComboBox<Double>();
		comboBoxAnguloEntrada.setBounds(88, 123, 71, 24);
		panel_5.add(comboBoxAnguloEntrada);
		
		JLabel lblNewLabel = new JLabel("ENTRADAS");
		lblNewLabel.setBounds(114, 33, 94, 15);
		panel_5.add(lblNewLabel);
		
		btnCalcEntrada = new JButton("Calcular");
		btnCalcEntrada.setBounds(88, 203, 117, 25);
		panel_5.add(btnCalcEntrada);
		
		panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel.add(panel_6);
		
		JLabel lblNumOrdemHarmonicos = new JLabel("Número de Ordem de Harmônicos");
		lblNumOrdemHarmonicos.setBounds(52, 41, 237, 15);
		panel_6.add(lblNumOrdemHarmonicos);
		
		comboBoxNumOrdemHarm = new JComboBox<Integer>();
		comboBoxNumOrdemHarm.setBounds(140, 68, 92, 24);
		panel_6.add(comboBoxNumOrdemHarm);
		
		JLabel lblHarmnicos = new JLabel("Harmônicos");
		lblHarmnicos.setBounds(39, 149, 83, 15);
		panel_6.add(lblHarmnicos);
		
		comboBoxParidadeHarm = new JComboBox<String>();
		comboBoxParidadeHarm.setBounds(140, 144, 92, 24);
		panel_6.add(comboBoxParidadeHarm);
		
		btnOk = new JButton("Ok");
		btnOk.setBounds(115, 203, 117, 25);
		panel_6.add(btnOk);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 240, 1000, 240);
		panel_1.setLayout(new GridLayout(1, 3));
		contentPane.add(panel_1);
		
		panel_7 = new JPanel();
		panel_7.setLayout(null);
		panel_1.add(panel_7);
		graficoH1 = new GraphPanel(zeros);
		graficoH1.setBounds(12, 0, 294, 118);
		panel_7.add(graficoH1);
		
		JLabel lblAmplitude_1 = new JLabel("Amplitude");
		lblAmplitude_1.setBounds(12, 142, 76, 15);
		panel_7.add(lblAmplitude_1);
		
		comboBoxAmplitudeH1 = new JComboBox<Double>();
		comboBoxAmplitudeH1.setBounds(91, 137, 66, 24);
		panel_7.add(comboBoxAmplitudeH1);
		
		JLabel lblngulo = new JLabel("Ângulo");
		lblngulo.setBounds(12, 188, 70, 15);
		panel_7.add(lblngulo);
		
		comboBoxAnguloH1 = new JComboBox<Double>();
		comboBoxAnguloH1.setBounds(91, 183, 83, 24);
		panel_7.add(comboBoxAnguloH1);
		
		JLabel lblHarmnico = new JLabel("Harmônico");
		lblHarmnico.setBounds(175, 142, 83, 15);
		panel_7.add(lblHarmnico);
		
		comboBoxHarmH1 = new JComboBox<Double>();
		comboBoxHarmH1.setBounds(263, 137, 58, 24);
		panel_7.add(comboBoxHarmH1);
		
		panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_1.add(panel_8);
		graficoH2 = new GraphPanel(zeros);
		graficoH2.setBounds(12, 0, 294, 118);
		panel_8.add(graficoH2);
		
		JLabel label_2 = new JLabel("Amplitude");
		label_2.setBounds(12, 145, 76, 15);
		panel_8.add(label_2);
		
		comboBoxAmplitudeH2 = new JComboBox<Double>();
		comboBoxAmplitudeH2.setBounds(91, 140, 66, 24);
		panel_8.add(comboBoxAmplitudeH2);
		
		JLabel label_3 = new JLabel("Ângulo");
		label_3.setBounds(12, 191, 70, 15);
		panel_8.add(label_3);
		
		comboBoxAnguloH2 = new JComboBox<Double>();
		comboBoxAnguloH2.setBounds(91, 186, 83, 24);
		panel_8.add(comboBoxAnguloH2);
		
		JLabel label_4 = new JLabel("Harmônico");
		label_4.setBounds(175, 145, 83, 15);
		panel_8.add(label_4);
		
		comboBoxHarmH2 = new JComboBox<Double>();
		comboBoxHarmH2.setBounds(255, 140, 66, 24);
		panel_8.add(comboBoxHarmH2);
		
		panel_9 = new JPanel();
		panel_9.setLayout(null);
		panel_1.add(panel_9);
		graficoH3 = new GraphPanel(zeros);
		graficoH3.setBounds(12, 0, 294, 118);
		panel_9.add(graficoH3);
		
		JLabel label_5 = new JLabel("Amplitude");
		label_5.setBounds(12, 147, 76, 15);
		panel_9.add(label_5);
		
		comboBoxAmplitudeH3 = new JComboBox<Double>();
		comboBoxAmplitudeH3.setBounds(91, 142, 66, 24);
		panel_9.add(comboBoxAmplitudeH3);
		
		JLabel label_6 = new JLabel("Ângulo");
		label_6.setBounds(12, 193, 70, 15);
		panel_9.add(label_6);
		
		comboBoxAnguloH3 = new JComboBox<Double>();
		comboBoxAnguloH3.setBounds(91, 188, 83, 24);
		panel_9.add(comboBoxAnguloH3);
		
		JLabel label_7 = new JLabel("Harmônico");
		label_7.setBounds(175, 147, 83, 15);
		panel_9.add(label_7);
		
		comboBoxHarmH3 = new JComboBox<Double>();
		comboBoxHarmH3.setBounds(260, 142, 61, 24);
		panel_9.add(comboBoxHarmH3);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 475, 1000, 245);
		panel_2.setLayout(new GridLayout(1, 3));
		contentPane.add(panel_2);
		
		panel_10 = new JPanel();
		panel_10.setLayout(null);
		panel_2.add(panel_10);
		graficoH4 = new GraphPanel(zeros);
		graficoH4.setBounds(12, 0, 294, 118);
		panel_10.add(graficoH4);
		
		JLabel label_8 = new JLabel("Amplitude");
		label_8.setBounds(12, 155, 76, 15);
		panel_10.add(label_8);
		
		comboBoxAmplitudeH4 = new JComboBox<Double>();
		comboBoxAmplitudeH4.setBounds(91, 150, 66, 24);
		panel_10.add(comboBoxAmplitudeH4);
		
		JLabel label_9 = new JLabel("Ângulo");
		label_9.setBounds(12, 201, 70, 15);
		panel_10.add(label_9);
		
		comboBoxAnguloH4 = new JComboBox<Double>();
		comboBoxAnguloH4.setBounds(91, 196, 83, 24);
		panel_10.add(comboBoxAnguloH4);
		
		JLabel label_10 = new JLabel("Harmônico");
		label_10.setBounds(175, 155, 83, 15);
		panel_10.add(label_10);
		
		comboBoxHarmH4 = new JComboBox<Double>();
		comboBoxHarmH4.setBounds(255, 150, 66, 24);
		panel_10.add(comboBoxHarmH4);
		
		panel_11 = new JPanel();
		panel_11.setLayout(null);
		panel_2.add(panel_11);
		graficoH5 = new GraphPanel(zeros);
		graficoH5.setBounds(12, 0, 294, 118);
		panel_11.add(graficoH5);
		
		JLabel label_11 = new JLabel("Amplitude");
		label_11.setBounds(12, 151, 76, 15);
		panel_11.add(label_11);
		
		comboBoxAmplitudeH5 = new JComboBox<Double>();
		comboBoxAmplitudeH5.setBounds(91, 146, 66, 24);
		panel_11.add(comboBoxAmplitudeH5);
		
		JLabel label_12 = new JLabel("Ângulo");
		label_12.setBounds(12, 197, 70, 15);
		panel_11.add(label_12);
		
		comboBoxAnguloH5 = new JComboBox<Double>();
		comboBoxAnguloH5.setBounds(91, 192, 83, 24);
		panel_11.add(comboBoxAnguloH5);
		
		JLabel label_13 = new JLabel("Harmônico");
		label_13.setBounds(175, 151, 83, 15);
		panel_11.add(label_13);
		
		comboBoxHarmH5 = new JComboBox<Double>();
		comboBoxHarmH5.setBounds(255, 146, 66, 24);
		panel_11.add(comboBoxHarmH5);
		
		panel_12 = new JPanel();
		panel_12.setLayout(null);
		panel_2.add(panel_12);
		graficoH6 = new GraphPanel(zeros);
		graficoH6.setBounds(12, 0, 294, 118);
		panel_12.add(graficoH6);
		
		JLabel label_14 = new JLabel("Amplitude");
		label_14.setBounds(12, 149, 76, 15);
		panel_12.add(label_14);
		
		comboBoxAmplitudeH6 = new JComboBox<Double>();
		comboBoxAmplitudeH6.setBounds(91, 144, 66, 24);
		panel_12.add(comboBoxAmplitudeH6);
		
		JLabel label_15 = new JLabel("Ângulo");
		label_15.setBounds(12, 195, 70, 15);
		panel_12.add(label_15);
		
		comboBoxAnguloH6 = new JComboBox<Double>();
		comboBoxAnguloH6.setBounds(91, 190, 83, 24);
		panel_12.add(comboBoxAnguloH6);
		
		JLabel label_16 = new JLabel("Harmônico");
		label_16.setBounds(175, 149, 83, 15);
		panel_12.add(label_16);
		
		comboBoxHarmH6 = new JComboBox<Double>();
		comboBoxHarmH6.setBounds(263, 144, 58, 24);
		panel_12.add(comboBoxHarmH6);
		
		panel_3 = new JPanel();
		panel_3.setBounds(10, 715, 1000, 245);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		graficoSaida = new GraphPanel(zeros);
		graficoSaida.setBounds(12, 49, 689, 126);
		panel_3.add(graficoSaida);
		
		JLabel lblSaidas = new JLabel("SAIDAS");
		lblSaidas.setBounds(414, 12, 70, 15);
		panel_3.add(lblSaidas);
		
		btnResultado = new JButton("Resultado");
		btnResultado.setVisible(false);;
		btnResultado.setBounds(871, 7, 117, 25);
		panel_3.add(btnResultado);
		
		lblNewLabel_1 = new JLabel("Serie de Fourrier Amplitude - Fase");
		lblNewLabel_1.setBounds(719, 88, 255, 15);
		panel_3.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(740, 160, 222, 15);
		panel_3.add(lblNewLabel_2);
		
		btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(12, 187, 117, 25);
		panel_3.add(btnVoltar);
		
		//Inicialização dos valores das JComboBoxes
		for(double i = 0; i < 221; i++) { 
			comboBoxAmplitudeEntrada.addItem(i);;
			comboBoxAmplitudeH1.addItem(i);
			comboBoxAmplitudeH2.addItem(i);
			comboBoxAmplitudeH3.addItem(i);
			comboBoxAmplitudeH4.addItem(i);
			comboBoxAmplitudeH5.addItem(i);
			comboBoxAmplitudeH6.addItem(i);
		}
		for(double i = -180; i < 181; i++) {
			comboBoxAnguloEntrada.addItem(i);
			comboBoxAnguloH1.addItem(i);
			comboBoxAnguloH2.addItem(i);
			comboBoxAnguloH3.addItem(i);
			comboBoxAnguloH4.addItem(i);
			comboBoxAnguloH5.addItem(i);
			comboBoxAnguloH6.addItem(i);
		}
		for(int i = 1; i < 7; i++) {
			comboBoxNumOrdemHarm.addItem(i);	
		}
		comboBoxParidadeHarm.addItem("Ímpares");
		comboBoxParidadeHarm.addItem("Pares");
		
		//Panels dos harmônicos colocados como invisíveis
		panel_6.setVisible(false);
		panel_7.setVisible(false);
		panel_8.setVisible(false);
		panel_9.setVisible(false);
		panel_10.setVisible(false);
		panel_11.setVisible(false);
		panel_12.setVisible(false);
		
	}
	
	
	//Métodos acessores para manipulação das informações
	public void setComboBoxHarmonicos(String tipo) {
		comboBoxHarmH1.removeAllItems();
		comboBoxHarmH2.removeAllItems();
		comboBoxHarmH3.removeAllItems();
		comboBoxHarmH4.removeAllItems();
		comboBoxHarmH5.removeAllItems();
		comboBoxHarmH6.removeAllItems();
		if(tipo == "Ímpares") {
			for(double i =0; i < 15; i++) {
				if(!(i%2 == 0)) {
					comboBoxHarmH1.addItem(i);
					comboBoxHarmH2.addItem(i);
					comboBoxHarmH3.addItem(i);
					comboBoxHarmH4.addItem(i);
					comboBoxHarmH5.addItem(i);
					comboBoxHarmH6.addItem(i);
				}
			}
		}else {
			for(double i =0; i < 15; i++) {
				if(i%2 == 0) {
					comboBoxHarmH1.addItem(i);
					comboBoxHarmH2.addItem(i);
					comboBoxHarmH3.addItem(i);
					comboBoxHarmH4.addItem(i);
					comboBoxHarmH5.addItem(i);
					comboBoxHarmH6.addItem(i);
				}
			}
		}
	}
	
	public void setGraficoEntrada(ArrayList<Double> pontosEntrada) {
		panel_4.removeAll();
		graficoEntrada = new GraphPanel(pontosEntrada);
		graficoEntrada.setBounds(12, 12, 309, 216);
		panel_4.add(graficoEntrada);
		graficoEntrada.revalidate();
		graficoEntrada.repaint();
	}
	public void setGraficoSaida(ArrayList<Double> pontosSaida) {
		//panel_3.removeAll();
		graficoSaida = new GraphPanel(pontosSaida);
		graficoSaida.setBounds(12, 49, 689, 126);
		panel_3.add(graficoSaida);
		graficoSaida.revalidate();
		graficoSaida.repaint();
	}
	public void setGraficoH1(ArrayList<Double> pontosH1) {
		//panel_3.removeAll();
		graficoH1 = new GraphPanel(pontosH1);
		graficoH1.setBounds(12, 0, 294, 118);
		panel_7.add(graficoH1);
		graficoH1.revalidate();
		graficoH1.repaint();
	}
	public void setGraficoH2(ArrayList<Double> pontosH2) {
		//panel_3.removeAll();
		graficoH2 = new GraphPanel(pontosH2);
		graficoH2.setBounds(12, 0, 294, 118);
		panel_8.add(graficoH2);
		graficoH2.revalidate();
		graficoH2.repaint();
	}
	public void setGraficoH3(ArrayList<Double> pontosH3) {
		//panel_3.removeAll();
		graficoH3 = new GraphPanel(pontosH3);
		graficoH3.setBounds(12, 0, 294, 118);
		panel_9.add(graficoH3);
		graficoH3.revalidate();
		graficoH3.repaint();
	}
	public void setGraficoH4(ArrayList<Double> pontosH4) {
		//panel_3.removeAll();
		graficoH4 = new GraphPanel(pontosH4);
		graficoH4.setBounds(12, 0, 294, 118);
		panel_10.add(graficoH4);
		graficoH4.revalidate();
		graficoH4.repaint();
	}
	public void setGraficoH5(ArrayList<Double> pontosH5) {
		//panel_3.removeAll();
		graficoH5 = new GraphPanel(pontosH5);
		panel_11.add(graficoH5);
		graficoH5.revalidate();
		graficoH5.repaint();
	}
	public void setGraficoH6(ArrayList<Double> pontosH6) {
		//panel_3.removeAll();
		graficoH6 = new GraphPanel(pontosH6);
		graficoH6.setBounds(12, 0, 294, 118);
		panel_12.add(graficoH6);
		graficoH6.revalidate();
		graficoH6.repaint();
	}

	public double getComboBoxAmplitudeEntrada() {
		return (double) comboBoxAmplitudeEntrada.getSelectedItem();
	}

	public double getComboBoxAnguloEntrada() {
		return (double) comboBoxAnguloEntrada.getSelectedItem();
	}

	public int getComboBoxNumOrdemHarm() {
		return (int) comboBoxNumOrdemHarm.getSelectedItem();
	}

	public String getComboBoxParidadeHarm() {
		return (String) comboBoxParidadeHarm.getSelectedItem();
	}

	public double getComboBoxAmplitudeH1() {
		return (double) comboBoxAmplitudeH1.getSelectedItem();
	}

	public double getComboBoxAnguloH1() {
		return (double) comboBoxAnguloH1.getSelectedItem();
	}

	public double getComboBoxHarmH1() {
		return (double) comboBoxHarmH1.getSelectedItem();
	}

	public double getComboBoxAmplitudeH2() {
		return (double) comboBoxAmplitudeH2.getSelectedItem();
	}

	public double getComboBoxAnguloH2() {
		return (double) comboBoxAnguloH2.getSelectedItem();
	}

	public double getComboBoxHarmH2() {
		return (double) comboBoxHarmH2.getSelectedItem();
	}

	public double getComboBoxAmplitudeH3() {
		return (double) comboBoxAmplitudeH3.getSelectedItem();
	}

	public double getComboBoxAnguloH3() {
		return (double) comboBoxAnguloH3.getSelectedItem();
	}

	public double getComboBoxHarmH3() {
		return (double) comboBoxHarmH3.getSelectedItem();
	}

	public double getComboBoxAmplitudeH4() {
		return (double) comboBoxAmplitudeH4.getSelectedItem();
	}

	public double getComboBoxAnguloH4() {
		return (double) comboBoxAnguloH4.getSelectedItem();
	}

	public double getComboBoxHarmH4() {
		return (double) comboBoxHarmH4.getSelectedItem();
	}

	public double getComboBoxAmplitudeH5() {
		return (double) comboBoxAmplitudeH5.getSelectedItem();
	}

	public double getComboBoxAnguloH5() {
		return (double) comboBoxAnguloH5.getSelectedItem();
	}

	public double getComboBoxHarmH5() {
		return (double) comboBoxHarmH5.getSelectedItem();
	}

	public double getComboBoxAmplitudeH6() {
		return (double) comboBoxAmplitudeH6.getSelectedItem();
	}

	public double getComboBoxAnguloH6() {
		return (double) comboBoxAnguloH6.getSelectedItem();
	}

	public double getComboBoxHarmH6() {
		return (double) comboBoxHarmH6.getSelectedItem();
	}
	public void setVisibilidadeMenuHarmonico() {
		panel_6.setVisible(true);
	}
	public void setVisibilidadeBtnResultado() {
		btnResultado.setVisible(true);
	}
	
	//Método para mostrar exatamente a quantidade de harmônicos selecionados pelo usuário
	public void atualizaVisibilidade(int valor) throws ValidationException {
		if(valor > 6 || valor < 1) {
			throw new ValidationException("Valor de Ordem de Harmônicos inválido");
		}
		switch(valor) {
		case 1:
			panel_7.setVisible(true);
			panel_8.setVisible(false);
			panel_9.setVisible(false);
			panel_10.setVisible(false);
			panel_11.setVisible(false);
			panel_12.setVisible(false);
			break;
		case 2:
			panel_7.setVisible(true);
			panel_8.setVisible(true);
			panel_9.setVisible(false);
			panel_10.setVisible(false);
			panel_11.setVisible(false);
			panel_12.setVisible(false);
			break;
		case 3:
			panel_7.setVisible(true);
			panel_8.setVisible(true);
			panel_9.setVisible(true);
			panel_10.setVisible(false);
			panel_11.setVisible(false);
			panel_12.setVisible(false);
			break;
		case 4:
			panel_7.setVisible(true);
			panel_8.setVisible(true);
			panel_9.setVisible(true);
			panel_10.setVisible(true);
			panel_11.setVisible(false);
			panel_12.setVisible(false);
			break;
		case 5:
			panel_7.setVisible(true);
			panel_8.setVisible(true);
			panel_9.setVisible(true);
			panel_10.setVisible(true);
			panel_11.setVisible(true);
			panel_12.setVisible(false);
			break;
		case 6:
			panel_7.setVisible(true);
			panel_8.setVisible(true);
			panel_9.setVisible(true);
			panel_10.setVisible(true);
			panel_11.setVisible(true);
			panel_12.setVisible(true);
			break;
		}
	}
	
	//Chamado dos Action listeners dos botões
	public void addBtnCalcEntradaListener(ActionListener Listener) {
		btnCalcEntrada.addActionListener(Listener);
	}
	
	public void addBtnResultadoListener(ActionListener Listener) {
		btnResultado.addActionListener(Listener);
	}
	
	public void addBtnOkListener(ActionListener Listener) {
		btnOk.addActionListener(Listener);
	}
	public void addBtnVoltarListener(ActionListener Listener) {
		btnVoltar.addActionListener(Listener);
	}
}


